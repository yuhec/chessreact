import React, { Component } from 'react';
import './App.css';
import MyTheme from './MyTheme';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import { withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import Cookies from 'js-cookie'

import Auth from './services/Auth'
import Users from './services/Users'

const styles = theme => ({
    textField: {
      width: 200,
    },
    button: {
        margin: theme.spacing.unit,
    },
});

class SignUp extends Component {
    state = {
        pseudo: '',
        email: '',
        password: '',
        open: false,
        errorText: '',
    }

    handleChange = name => event => {
        this.setState({
          [name]: event.target.value,
        });
      };

      handleSubmit = async event => {
        if (this.state.password.trim().length > 0 && this.state.email.trim().length > 0) {
            let user = await Users.users(this.state.pseudo, this.state.email, this.state.password)
            
            if (user) {
                if (Cookies.get('jwt')) {
                    this.props.history.push('/')
                } else {
                    this.setState({ open: true, errorText: 'La connexion a échoué', ...event.persist() })
                }
            } else {
                //ERROR 500 VIOLATION CONSTRAINT
                this.setState({ open: true, errorText: 'Pseudo ou email déjà pris', ...event.persist() })
            }

            
        } else {
            this.setState({ open: true, errorText: 'Champs manquants', ...event.persist() })
        }
    }
    handleClose = () => {
        this.setState({ open: false })
    }

    render() {
        const { classes } = this.props
        const { open, errorText } = this.state
        return (
            <div className="App">
                <MuiThemeProvider theme={MyTheme}>
                    <h1>Formulaire d'inscription</h1>
                    <form>
                        <TextField
                            label="Pseudo"
                            value={this.state.pseudo}
                            onChange={this.handleChange('pseudo')}
                            className={classes.textField}
                            margin="normal"
                            required
                        />
                        <TextField
                            label="Email"
                            value={this.state.email}
                            onChange={this.handleChange('email')}
                            className={classes.textField}
                            margin="normal"
                            required
                        />
                        <TextField 
                            label="Mot de passe"
                            value={this.state.password}
                            onChange={this.handleChange('password')}
                            className={classes.textField}
                            margin="normal"
                            type="password"
                            autoComplete="current-password"
                            required
                        />
                        <Button className={classes.button} onClick={this.handleSubmit} variant="contained" color="primary">S'inscrire</Button>
                    </form>
                    <Snackbar
                        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                        open={open}
                        onClose={this.handleClose}
                        ContentProps={{
                            'aria-describedby': 'message-id',
                        }}
                        message={<span id="message-id">{errorText}</span>}
                    />
                </MuiThemeProvider>
            </div>
        );
    }
}

SignUp.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SignUp);
