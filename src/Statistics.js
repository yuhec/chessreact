import React, { Component } from 'react';
import './App.css';
import MyTheme from './MyTheme';
import { withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import ListBestPlayer from "./ListBestPlayer";
import PieUserStats from "./PieUserStats";
import ColumnUserStats from "./ColumnUserStats";



const styles = theme => ({
    textField: {
        width: 200,
    },
    button: {
        margin: theme.spacing.unit,
    },
});



function getUser(){
    return {coupMoyen: 28, score: 29}
}

class Statistics extends Component {
    state = {
        user: getUser(),
        isConnected: true
    }


    render() {
        const {isConnected} = this.state;
        if(isConnected){
            return (
                <div className="App">
                    <MuiThemeProvider theme={MyTheme}>
                        <h1>Statistiques</h1>
                        <div className="listStats">
                            <ListBestPlayer/>
                            <div>
                                <PieUserStats/>
                                <br/>
                                <ColumnUserStats/>
                            </div>
                        </div>
                    </MuiThemeProvider>
                </div>
            );
        }else {
            return (
                <div className="App">
                    <MuiThemeProvider theme={MyTheme}>
                        <h1>Statistiques</h1>
                        <ListBestPlayer/>
                    </MuiThemeProvider>
                </div>
            );
        }
    }
}

export default withStyles(styles)(Statistics);
