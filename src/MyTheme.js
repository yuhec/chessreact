import { createMuiTheme } from '@material-ui/core/styles';

export default createMuiTheme({
    typography: {
        useNextVariants: true,
      },
    palette: {
        primary: {
            light: '#616161',
            main: '#424242',
            dark: '#212121',
            contrastText: '#fff',
        },
        secondary: {
            light: '#F5F5F5',
            main: '#BDBDBD',
            dark: '#424242',
            contrastText: '#000',
        },
    },
});