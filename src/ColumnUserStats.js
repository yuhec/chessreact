import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import ReactFC from 'react-fusioncharts';
import FusionCharts from 'fusioncharts';
import Column3D from 'fusioncharts/fusioncharts.charts';
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';

ReactFC.fcRoot(FusionCharts, Column3D, FusionTheme);

const dataMensuelJson = {
    type: 'column3d',// The chart type
    width: '60%', // Width of the chart
    height: '40%', // Height of the chart
    dataFormat: 'json', // Data type
    dataSource: {
        // Chart Configuration
        "chart": {
            "caption": "Votre evolution des parties gagnées",
            "subCaption": "par mois",
            "xAxisName": "Mois",
            "yAxisName": "Parties Gagnées",
            "numberSuffix": " parties",
            "theme": "fusion",
            "captionFontBold": 1,
            "captionFontColor": "#993300",
            "subcaptionFontColor": "#bd4800",
            "baseFontColor": "#004c98",
            "yAxisNameFontColor": "#993300",
            "yAxisNameFontBold": "1",
            "xAxisNameFontColor": "#993300",
            "xAxisNameFontBold": "1",
        },
        // Chart Data
        "data": [{
            "label": "Jan.",
            "value": "15"
        }, {
            "label": "Fev.",
            "value": "36"
        }, {
            "label": "Mars",
            "value": "9"
        }, {
            "label": "Avr.",
            "value": "13"
        }, {
            "label": "Mai",
            "value": "11"
        }, {
            "label": "Juin",
            "value": "25"
        }, {
            "label": "Juil.",
            "value": "30"
        }, {
            "label": "Aout",
            "value": "31"
        },{
            "label": "Sept.",
            "value": "34"
        }, {
            "label": "Oct.",
            "value": "25"
        }, {
            "label": "Nov.",
            "value": "13"
        }, {
            "label": "Dec.",
            "value": "27"
        }]
    }
};

const styles = theme => ({
    root: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit *2,
    },
});


function getColumnUserStats(props) {
    const { classes } = props;
    return (
        <Paper className={classes.root} elevation={5}>
            <ReactFC {...dataMensuelJson}/>
        </Paper>
    );
}

getColumnUserStats.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(getColumnUserStats);
