import React, { Component } from 'react';
import './App.css';
import MyTheme from './MyTheme';

import PropTypes from 'prop-types';
import { withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import IconHome from '@material-ui/icons/Home';
import { Link } from 'react-router-dom'


const styles = {
    root: {
      flexGrow: 1,
    },
    grow: {
      flexGrow: 1,
    },
    menuButton: {
      marginLeft: -12,
      marginRight: 20,
    },
};


  
class Header extends Component {
  render() {
    const { classes } = this.props;
      return (
        <div>
          <MuiThemeProvider theme={MyTheme}>
            <AppBar position="static">
              <Toolbar variant="dense">
              <Link to="/" style={{ textDecoration: 'none', color: 'white' }}>
                  <IconButton className={classes.menuButton} color="inherit" >
                    <IconHome/>
                  </IconButton>
                  </Link>
                <Typography variant="h6" color="inherit">
                  Super Chess Game
                </Typography>
              </Toolbar>
            </AppBar>
          </MuiThemeProvider>
        </div>
      );
  }
}

Header.propTypes = {
    classes: PropTypes.object.isRequired,
  };

  export default withStyles(styles)(Header);
