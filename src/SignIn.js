import React, { Component } from 'react';
import './App.css';
import MyTheme from './MyTheme';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import { withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import Cookies from 'js-cookie'

import Auth from './services/Auth'


const styles = theme => ({
    textField: {
      width: 200,
    },
    button: {
        margin: theme.spacing.unit,
    },
});

class SignIn extends Component {
    state = {
        email: '',
        password: '',
        open: false,
        textError: '',
    }

    handleChange = name => event => {
        this.setState({
          [name]: event.target.value,
        });
      };

    handleSubmit = async event => {
        if (this.state.password.trim().length > 0 && this.state.email.trim().length > 0) {
            await Auth.auth(this.state.email, this.state.password)
            
            if (Cookies.get('jwt')) {
                this.props.history.push('/')
            } else {
                this.setState({ open: true, textError: 'La connexion a échoué', ...event.persist() })
            }
        } else {
            this.setState({ open: true, textError: 'Champs manquants', ...event.persist() })
        }
    }
    handleClose = () => {
        this.setState({ open: false })
    }
    render() {
        const { classes } = this.props
        const { open } = this.state
        return (
            <div className="App">
                <MuiThemeProvider theme={MyTheme}>
                    <h1>Veuillez vous connecter pour jouer!</h1>
                    <form>
                        <TextField
                            name="email"
                            label="Email"
                            value={this.state.email}
                            onChange={this.handleChange('email')}
                            className={classes.textField}
                            margin="normal"
                            required
                        />
                        <TextField 
                            name="password"
                            label="Mot de passe"
                            value={this.state.password}
                            onChange={this.handleChange('password')}
                            className={classes.textField}
                            margin="normal"
                            type="password"
                            autoComplete="current-password"
                            required
                        />
                        <Button className={classes.button} onClick={this.handleSubmit}  variant="contained" color="primary">Connexion</Button>
                    </form>
                    <Snackbar
                        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                        open={open}
                        onClose={this.handleClose}
                        ContentProps={{
                            'aria-describedby': 'message-id',
                        }}
                        message={<span id="message-id">Champs incorrects</span>}
                    />
                </MuiThemeProvider>
            </div>
        );
    }
}

SignIn.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SignIn);
