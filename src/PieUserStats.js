import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import ReactFC from 'react-fusioncharts';
import FusionCharts from 'fusioncharts';
import pie2D from 'fusioncharts/fusioncharts.charts';
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';

ReactFC.fcRoot(FusionCharts, pie2D, FusionTheme);

const dataRatioJson = {
    type: 'pie2d',
    width: '35%',
    height: '40%',
    dataFormat: 'json',
    dataSource: {
        "chart": {
            "caption": "Votre ratio victoire / défaite",
            "theme": "fusion",
            "captionFontColor": "#993300",
            "captionFontBold": 1,
            "bgAlpha": "75",
        },
        "data": [
            {
                "label": "Victoire",
                "value": "287"
            },
            {
                "label": "Défaite",
                "value": "203"
            }
        ]
    }
};

const styles = theme => ({
    root: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit *2,
    },
});



function getPieUserStats(props) {
    const { classes } = props;
    return (
            <Paper className={classes.root} elevation={5}>
                <ReactFC {...dataRatioJson}/>
            </Paper>
    );
}

getPieUserStats.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(getPieUserStats);
