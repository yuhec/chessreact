import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import StarIcon from '@material-ui/icons/Star';

const styles = theme => ({
    root: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit *2,
    }
});
const fakeList = []

function createFakeplayer() {
    for(let a = 0; a< 10; a++){
        fakeList.push({id: a, name: "playerNumber"+(a+1), score: "Score : "+ (10*10-a*a)})
    }
}

function getListBestPlayer(props) {
    createFakeplayer();
    const { classes } = props;
    return (
        <Paper className={classes.root} elevation={5}>
            <h5>Liste des meilleurs joueurs</h5>
            <List component="nav">
                <ListItem button>
                    <ListItemIcon>
                        <StarIcon />
                    </ListItemIcon>
                    <ListItemText inset primary= {fakeList[0].name} secondary={fakeList[0].score}/>
                </ListItem>
                <ListItem button>
                    <ListItemText inset primary= {fakeList[1].name} secondary={fakeList[1].score}/>
                </ListItem>
                <ListItem button>
                    <ListItemText inset primary= {fakeList[2].name} secondary={fakeList[2].score}/>
                </ListItem>
                <ListItem button>
                    <ListItemText inset primary= {fakeList[3].name} secondary={fakeList[3].score}/>
                </ListItem>
                <ListItem button>
                    <ListItemText inset primary= {fakeList[4].name} secondary={fakeList[4].score}/>
                </ListItem>
                <ListItem button>
                    <ListItemText inset primary= {fakeList[5].name} secondary={fakeList[5].score}/>
                </ListItem>
                <ListItem button>
                    <ListItemText inset primary= {fakeList[6].name} secondary={fakeList[6].score}/>
                </ListItem>
                <ListItem button>
                    <ListItemText inset primary= {fakeList[7].name} secondary={fakeList[7].score}/>
                </ListItem>
                <ListItem button>
                    <ListItemText inset primary= {fakeList[8].name} secondary={fakeList[8].score}/>
                </ListItem>
                <ListItem button>
                    <ListItemText inset primary= {fakeList[9].name} secondary={fakeList[9].score}/>
                </ListItem>

            </List>
        </Paper>
    );
}

getListBestPlayer.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(getListBestPlayer);
