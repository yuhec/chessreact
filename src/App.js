import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import HomePage from './HomePage'
import SignIn from './SignIn'
import SignUp from './SignUp'
import Header from "./Header";
import Chessboard from './chessboardGame/Chessboard';
import Statistics from "./Statistics";



class App extends Component {
  render() {
    return (
      <Router>
          <div>
              <Route path="/" component={Header}/>
              <Route exact path="/" component={HomePage}/>
              <Route exact path="/SignIn" component={SignIn}/>
              <Route exact path="/SignUp" component={SignUp}/>
              <Route path="/Game" component={Chessboard}/>
              <Route exact path="/Statistics" component={Statistics}/>
          </div>
      </Router>
    );
  }
}

export default App;
//              <img src={ require('./images/logo/chessLogo.png')} alt="black King"/>
