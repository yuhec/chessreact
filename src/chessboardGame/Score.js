import React, { Component } from 'react';
import '../App.css';
import { withStyles } from '@material-ui/core/styles';


const styles = theme => ({
  displayBlackPlayer: {
    color: 'black'
  }
});

class Score extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className="score">
        <p>
          {/* {this.props.playerTurn === 'white' ? <b>{this.props.whiteUserPseudo}</b> : this.props.whiteUserPseudo} : {this.props.score.movesWhite} */}
          {this.props.playerTurn === 'white' ? <b>White</b> : 'White'} : {this.props.score.movesWhite}
        </p>
        <p className={classes.displayBlackPlayer}>
          {/* {this.props.playerTurn === 'black' ? <b>{this.props.blackUserPseudo}</b> : this.props.blackUserPseudo} : {this.props.score.movesBlack} */}
          {this.props.playerTurn === 'black' ? <b>Black</b> : 'Black'} : {this.props.score.movesBlack}
        </p>
      </div>
    )
  }
}

export default withStyles(styles)(Score);
