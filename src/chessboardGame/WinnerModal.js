import React, { Component } from 'react';
import '../App.css';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';
import { withStyles } from '@material-ui/core/styles';
import WinnerBackground from '../images/winner-chess.jpg'
import  { Redirect } from 'react-router-dom'


const styles = theme => ({
    paper: {
      position: 'absolute',
      width: theme.spacing.unit * 50,
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing.unit * 4,
    },
});

var modal = {
  backgroundImage: `url(${WinnerBackground})`,
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover',
  backgroundPosition: 'center',
  width: '45%',
  height: '45%',
  top: '25%',
  left: '25%',
  textAlign: 'center'
}

class WinnerModal extends Component {
  state = {
    open: true,
  };
    
  handleOpen = () => {
    this.setState({ open: true });
  };
    
  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes } = this.props;
    if (!this.state.open) {
      return <Redirect to='/'/>
    }
    return (
      <div className="modal">
        <Modal
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div style={modal} className={classes.paper}>
            <Typography variant="h5" >
              <b>Le gagnant est : {this.props.winnerPseudo} !!!</b>
            </Typography>
          </div>
        </Modal>
      </div>
    )
  }
}

export default withStyles(styles)(WinnerModal);
