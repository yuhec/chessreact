import React, { Component } from 'react';
import '../App.css';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import { isEmpty } from 'lodash'

import blackRook from '../images/Sprites/blackRook.png'
import blackBishop from '../images/Sprites/blackBishop.png'
import blackKing from '../images/Sprites/blackKing.png'
import blackHorse from '../images/Sprites/blackKnight.png'
import blackPawn from '../images/Sprites/blackPawn.png'
import blackQueen from '../images/Sprites/blackQueen.png'

import whiteRook from '../images/Sprites/whiteRook.png'
import whiteBishop from '../images/Sprites/whiteBishop.png'
import whiteKing from '../images/Sprites/whiteKing.png'
import whiteHorse from '../images/Sprites/whiteKnight.png'
import whitePawn from '../images/Sprites/whitePawn.png'
import whiteQueen from '../images/Sprites/whiteQueen.png'


const styles = theme => ({
  paper: {
    display: 'flex',
    zIndex: 1,
    position: 'relative',
  },
});

class Chessboard extends Component {
  state = {
    rows: [0,1,2,3,4,5,6,7],
    moves: [],
    selected: {
      piece: {},
      selectedMoves: []
    }
  }

  createBoard = (row) => {
    const NB_SQUARES_COL = [0,1,2,3,4,5,6,7]
    let board = []
    let position = ''

    for (let i=0; i<NB_SQUARES_COL.length; i++) {

      let piece = this.insertPieces(row, NB_SQUARES_COL[i])
      position = row+''+NB_SQUARES_COL[i]

      if (row%2===0) {
        if (i%2 === 0) {
          board.push(<div className="square light" key={position} ref={position} onMouseOver={this.allowedMoves.bind(this, piece)} onMouseLeave={this.mouseOut.bind(this,position)} onClick={this.move.bind(this, piece, position)}> {piece ? <img alt={piece.className} src={piece.img}></img> : ''} </div>)
        } else {
          board.push(<div className="square dark" key={position} ref={position} onMouseOver={this.allowedMoves.bind(this, piece)} onMouseLeave={this.mouseOut.bind(this,position)} onClick={this.move.bind(this, piece, position)}>{piece ? <img alt={piece.className} src={piece.img}></img> : ''} </div>)
        }
      } else {
        if (i%2 !== 0) {
          board.push(<div className="square light" key={position} ref={position} onMouseOver={this.allowedMoves.bind(this, piece)} onMouseLeave={this.mouseOut.bind(this,position)} onClick={this.move.bind(this, piece, position)}>{piece ? <img alt={piece.className} src={piece.img}></img> : ''} </div>)
        } else {
          board.push(<div className="square dark" key={position} ref={position} onMouseOver={this.allowedMoves.bind(this, piece)} onMouseLeave={this.mouseOut.bind(this,position)} onClick={this.move.bind(this, piece, position)}>{piece ? <img alt={piece.className} src={piece.img}></img> : ''}</div>)
        }
      }
    }
    return board
  }

  showAllowedMoves (piece, selected) {

    if (piece && (this.props.playerTurn === piece.color) && (this.props.playerTurn === this.props.loggedUserColor) ) {
      let position = piece.coordY + '' + piece.coordX
      let squares = [this.refs[position]]

      piece.moves.forEach(move => {
        squares.push(this.refs[move.y+''+move.x])
      })

      squares.forEach(square => {
        if (selected) {
          square.classList.add('selected')
        } else {
          square.classList.add('allowed')
        }
      })

      if (selected) {
        this.setState( () => {
          return {
            selected: {
              piece: piece,
              selectedMoves: piece.moves
            }
          }
        })
      } else {
        this.setState({moves: piece.moves})
      }
    }
  }

  allowedMoves(piece) {
    this.showAllowedMoves(piece, false)
  }

  mouseOut(id) {
    let squares = [this.refs[id]]
    if (squares) {
      this.state.moves.forEach(move => {
        squares.push(this.refs[move.y+''+move.x])
      })
      squares.forEach(square => {
        square.classList.remove('allowed')
      })
    }
  }

  removeSelection() {
    let selectedPosition = this.state.selected.piece.coordY + '' + this.state.selected.piece.coordX
    this.refs[selectedPosition].classList.remove('selected')
    this.state.selected.selectedMoves.forEach(move => {
      this.refs[move.y+''+move.x].classList.remove('selected')
    })
    this.setState({selected: {}})
  }

  move(piece, position) {
    // If i've selected a piece for the first time

    if (piece && isEmpty(this.state.selected.piece)) {
      this.showAllowedMoves(piece, true)
    } 
    // If i've selected an other piece
    else if (piece && !isEmpty(this.state.selected.piece)) {
      let moveAccepted = false
      let x, y = ''
      this.state.selected.selectedMoves.forEach(move => {
        // If my new selection is somewhere i can move
        if (move.y+''+move.x === position) {
          moveAccepted = true
          x = move.x
          y = move.y
        }
      })
      // Then I move my piece and eat the other one
      if (moveAccepted) {
        let selectedPiece = this.state.selected.piece
        this.removeSelection() 
        
        //If Rook
        if ((selectedPiece.color === this.props.playerTurn) && 
        ((selectedPiece.className === 'Rook' && piece.className === 'King') ||
        (selectedPiece.className === 'King' && piece.className === 'Rook'))) {
          this.props.setPieces(selectedPiece, x, y, true) 
        } else {
          this.props.setPieces(selectedPiece, x, y, false)
        }
      } 
      // Or it's my new selection
      else {
        this.removeSelection()
        this.showAllowedMoves(piece, true)
      }
    }
    // If i've selected an empty square
    else if (!piece && !isEmpty(this.state.selected)) {
      let moveAccepted = false
      let x, y = ''
      this.state.selected.selectedMoves.forEach(move => {
        // If my new selection is somewhere i can move
        if (move.y+''+move.x === position) {
          moveAccepted = true
          x = move.x
          y = move.y
        }
      })
      if (moveAccepted) {
        let selectedPiece = this.state.selected.piece
        this.removeSelection()
        this.props.setPieces(selectedPiece, x, y) 
      } 
    }
  }

  insertPieces = (x, y) => {
    let pieces = this.props.pieces
    // let pieces = chessJson.pieces
    if (!isEmpty(pieces)) {
      if (pieces[x][y] != null) {

        //Associate the piece with it image
        if (pieces[x][y].color === 'black' && pieces[x][y].name === 'Bishop') {
          pieces[x][y].img = blackBishop
        } else if (pieces[x][y].color === 'black' && pieces[x][y].name === 'King') {
          pieces[x][y].img = blackKing
        } else if (pieces[x][y].color === 'black' && pieces[x][y].name === 'Horse') {
          pieces[x][y].img = blackHorse
        } else if (pieces[x][y].color === 'black' && pieces[x][y].name === 'Pawn') {
          pieces[x][y].img = blackPawn
        } else if (pieces[x][y].color === 'black' && pieces[x][y].name === 'Queen') {
          pieces[x][y].img = blackQueen
        } else if (pieces[x][y].color === 'black' && pieces[x][y].name === 'Rook') {
          pieces[x][y].img = blackRook
        } else if (pieces[x][y].color === 'white' && pieces[x][y].name === 'Bishop') {
          pieces[x][y].img = whiteBishop
        } else if (pieces[x][y].color === 'white' && pieces[x][y].name === 'King') {
          pieces[x][y].img = whiteKing
        } else if (pieces[x][y].color === 'white' && pieces[x][y].name === 'Horse') {
          pieces[x][y].img = whiteHorse
        } else if (pieces[x][y].color === 'white' && pieces[x][y].name === 'Pawn') {
          pieces[x][y].img = whitePawn
        } else if (pieces[x][y].color === 'white' && pieces[x][y].name === 'Queen') {
          pieces[x][y].img = whiteQueen
        } else if (pieces[x][y].color === 'white' && pieces[x][y].name === 'Rook') {
          pieces[x][y].img = whiteRook
        }
      }
      return pieces[x][y]
    }
   
  }

  render() {
    const { classes } = this.props
      return (
        <div className="grid">
          {this.state.rows.map(nb_row => (
            <Paper elevation={4} className={classes.paper} key={nb_row}>
              {this.createBoard(nb_row)}
            </Paper>
          ))}
        </div>
      )
  }
}

export default withStyles(styles)(Chessboard);
