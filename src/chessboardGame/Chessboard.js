import React, { Component } from 'react';
import '../App.css';
import ChessGrid from './ChessGrid'
import Score from './Score'
import WinnerModal from './WinnerModal'
import StalemateModal from './StalemateModal'
import { withStyles } from '@material-ui/core/styles';

import UsersApi from '../services/Users'
import DataApi from '../services/Games'
import chessJson from '../chess.json'
import Cookies from 'js-cookie'

import socketIOClient from 'socket.io-client';

const styles = theme => ({

});

class Chessboard extends Component {
  state = {
    pieces: {},
    score: {},
    gameplay: {},
    whiteUserPseudo: null,
    blackUserPseudo: null,
    loggedUserColor: null,
  }

  constructor(props) {
    super(props)

  }

  getData = async () => {
    if (this.state.gameplay.id &&
      this.state.whiteUserPseudo &&
      this.state.blackUserPseudo) {

      const { id } = this.state.gameplay
      const { pieces } = this.state

      DataApi.postGameData(id, pieces)
    } else {
      let data = await DataApi.createGame()
      this.setState({
        gameplay: data.gameplay,
        score: data.score,
        pieces: data.piecesArray,
      })
      this.props.history.push(`/Game/${data.gameplay.id}`)
    }
  }

  initGame = async () => {
    let data = await DataApi.createGame()

    await this.setState({
      gameplay: data.gameplay,
      score: data.score,
      pieces: data.piecesArray,
    })
    await this.props.history.push(`/Game/${data.gameplay.id}`)

    await this.getUsers()
    await this.getLoggedUserColor()
    return data
  }

  getUsers = async () => {
    if (this.state.gameplay.whitePlayerId) {
      let responseWhiteUser = await UsersApi.getUserById(this.state.gameplay.whitePlayerId)

      if (responseWhiteUser) {
        if (responseWhiteUser.pseudo !== this.state.whiteUserPseudo) {
          this.setState({ whiteUserPseudo: responseWhiteUser.pseudo })
        }
      }
    }

    if (this.state.gameplay.blackPlayerId) {
      let responseBlackUser = await UsersApi.getUserById(this.state.gameplay.blackPlayerId)
      
      if (responseBlackUser) {
        if (responseBlackUser.pseudo !== this.state.blackUserPseudo) {
          this.setState({ blackUserPseudo: responseBlackUser.pseudo })
        }
      }
    }
  }

  setPieces = (piece, newX, newY, isRook) => {
    let newPieces = this.state.pieces

    if (isRook) {
      newPieces[piece.coordY][piece.coordX] = newPieces[newY][newX]
      newPieces[piece.coordY][piece.coordX].coordX = piece.coordX
      newPieces[piece.coordY][piece.coordX].coordY = piece.coordY
    } else {
      // Delete the old position
      newPieces[piece.coordY][piece.coordX] = null
    }

    //Init the new position
    newPieces[newY][newX] = piece
    newPieces[newY][newX].coordX = newX
    newPieces[newY][newX].coordY = newY

    this.setState({ pieces: newPieces })
    const socket = socketIOClient("http://localhost:8000")

    let req = {
      pieces: this.state.pieces,
      gameplay: this.state.gameplay,
    }
    const cookie = Cookies.get('jwt');

    
    socket.emit("update_game", req, cookie)
    //DataApi.postGameDate(this.state.gameplay.id, newPieces)
  }

  getLoggedUserColor = async () => {
    let loggedUser = await UsersApi.getLoggedUser()
    
    if (this.state.gameplay.whitePlayerId == loggedUser.id) {
      this.setState({loggedUserColor: 'white'}) 
    } else if (this.state.gameplay.blackPlayerId == loggedUser.id) {
      this.setState({loggedUserColor: 'black'}) 
    }
  }
  getPlayerTurn = () => {
    if ((this.state.score.movesBlack + this.state.score.movesWhite) % 2 === 0) {
      // return `Au tour de ${this.state.whiteUserPseudo}`
      return `Au tour du joueur blanc`
    } else if ((this.state.score.movesBlack + this.state.score.movesWhite) % 2 !== 0) {
      // return `Au tour de ${this.state.blackUserPseudo}`
      return `Au tour du joueur noir`
    }
    
  }

  componentDidMount() {
    const socket = socketIOClient("http://localhost:8000")
    socket.on('update_game', (data) => {

      this.setState({
        gameplay: data.gameplay,
        score: data.score,
        pieces: data.piecesArray,
      })
    })
    socket.on('error', (err) => {
      console.log(err)
    })

    this.initGame().then(datas => {
      socket.emit('join_room', datas.gameplay.id)
    })
  }

  componentWillUnmount() {
    this.setState(() => {
      return {
        pieces: {},
        score: {},
        gameplay: {},
        whiteUserPseudo: null,
        blackUserPseudo: null,
        loggedUserColor: null,
      }
    })

    const socket = socketIOClient("http://localhost:8000");

    socket.emit('leave_room', this.state.gameplay.id)
    socket.emit('disconect')
  }

  render() {
    let winner

    if (this.state.score.isWhiteWinner != null) {
      if (this.state.score.isWhiteWinner) {
        winner = <WinnerModal winnerPseudo={this.state.whiteUserPseudo} />
      } else {
        winner = <WinnerModal winnerPseudo={this.state.blackUserPseudo} />
      }
    }

    let stalemate
    if (this.state.score.stalemate) {
      stalemate = <StalemateModal/>
    }

    let msg
    if (this.state.gameplay.blackPlayerId === null) {
      msg = 'En attente d\'un adversaire'
    } else {
      msg = this.getPlayerTurn()
    }

    let myColor
    if (this.state.loggedUserColor === 'white') {
      myColor = 'Vous êtes le joueur blanc'
    } else {
      myColor = 'Vous êtes le joueur noir'
    }

    return (
      <div className="App chessboard">
        <div className="game-rules">
          {msg}
        </div>
        <div className="game-info">
          {myColor}
        </div>

        <Score score={this.state.score}
          whiteUserPseudo={this.state.whiteUserPseudo}
          blackUserPseudo={this.state.blackUserPseudo}
          playerTurn={(this.state.score.movesBlack + this.state.score.movesWhite) % 2 === 0 ? 'white' : 'black'}
        />
        <ChessGrid pieces={this.state.pieces}
          setPieces={this.setPieces}
          loggedUserColor={this.state.loggedUserColor}
          playerTurn={(this.state.score.movesBlack + this.state.score.movesWhite) % 2 === 0 ? 'white' : 'black'}
        />

        {winner}
        {stalemate}

      </div>
    )
  }
}

export default withStyles(styles)(Chessboard);
