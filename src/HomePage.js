import React, { Component } from 'react';
import './App.css';
import Background from './images/logo/chess-background.jpg'
import { Link } from 'react-router-dom'
import Button from '@material-ui/core/Button';
import MyTheme from './MyTheme';
import { withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import Cookies from 'js-cookie'

var backgroundImage = {
    backgroundImage: `url(${Background})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center'
    
}

const styles = theme => ({
    button: {
      margin: theme.spacing.unit,
    },
    input: {
      display: 'none',
    },
});

class HomePage extends Component {

    disconnect = () => {
        Cookies.remove('jwt')
        this.props.history.push('/')
    }

    render() {
        const { classes } = this.props;
        let notConnectedlink, noAccountLink, desconnectedLink, playLink
        if (Cookies.get('jwt')) {
            desconnectedLink = 
                <Button variant="contained" color="primary" size="large" className={classes.button} onClick={this.disconnect}>Se déconnecter</Button>
            playLink =
                <Link to="/Game">
                    <Button variant="contained" color="primary" size="large" className={classes.button}>Jouer</Button>
                </Link>
        } else {
            notConnectedlink = 
                <Link to="/SignIn">
                    <Button variant="contained" color="primary" size="large" className={classes.button}>Se connecter</Button>
                </Link>
            noAccountLink = 
                <Link to="/SignUp">
                    <Button variant="contained" color="primary" size="large" className={classes.button}>S'inscrire</Button>
                </Link>
            
        }
        return (
            <div className="App" style={backgroundImage}>
                <MuiThemeProvider theme={MyTheme}>
                    {notConnectedlink}
                    {noAccountLink}
                    {playLink}
                    <Link to="/Statistics">
                        <Button variant="contained" color="primary" size="large" className={classes.button}>Statistiques</Button>
                    </Link>
                    {desconnectedLink}
                </MuiThemeProvider>
            </div>
        );
    }
}

export default withStyles(styles)(HomePage);
