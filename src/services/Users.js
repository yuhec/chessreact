
class Users {

  static users = async (pseudo, email, password) => {
    console.log("REACT : " + pseudo + email + password)
    try {
      return await fetch(`http://localhost:8000/users`, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        method: 'POST',
        body: JSON.stringify({ pseudo, email, password })
      })
    } catch (e) {
      console.log(e)
    }
  }
  static getUserById = async (id) => {
    try {
      let res = null
      return await fetch(`http://localhost:8000/users/${id}}`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
      })
      // .then(async datas => {
      //   res =  await datas.json()
      //   console.log(res)
      // })
      // .catch(err=> {
      //   console.log(err)
      // })

      // console.log(res)
      // return res;

    } catch (e) {
      console.log(e)
    }
  }
  static getLoggedUser = async () => {
    try {
      let res = null
      await fetch(`http://localhost:8000/users/loggedIn`).then(async datas => {
        res = await datas.json()
      }).catch(err => {
        console.log(err)
      })
      
      console.log('LALA',res)
      return res
    } catch (e) {
      console.log(e)
    }
  }
}

export default Users