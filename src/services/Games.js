
class Games {

  static postGameDate = async (gameplayId, pieces) => {
    try {
      fetch(`http://localhost:8000/gameplayChessboardScore`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        mode: "cors",
        body: JSON.stringify({ pieces, gameplayId }),
      })
    } catch (e) {
      console.log(e)
    }
  }
  static createGame = async () => {
    try {
      let res = null;

      await fetch(`http://localhost:8000/gameplayChessboardScore/createGame`, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        method: 'PUT',
      }).then(async datas => {
        res =  await datas.json()
      }).catch(err=> {
        console.log(err)
      })

      return res;
    } catch (err) {
      console.log(err)
    }
  }
}

export default Games