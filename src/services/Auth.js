
class Auth {

  static auth = async (email, password) => {
    try {
      return await fetch(`http://localhost:8000/auth/connect`, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        method: 'POST', 
        body: JSON.stringify({email, password})
      })
    } catch (e) {
      console.log(e)
    }
  }
}

export default Auth